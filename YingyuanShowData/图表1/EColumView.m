//
//  EColumView.m
//  XYPieChart
//
//  Created by 思来氏 on 14-9-25.
//  Copyright (c) 2014年 Xiaoyang Feng. All rights reserved.
//

#import "EColumView.h"
#import "EColumnDataModel.h"
#import "EColumnChartLabel.h"
#import "EFloatBox.h"
#import "EColor.h"
#include <stdlib.h>


@interface EColumView ()

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) EFloatBox *eFloatBox;

@property (nonatomic, strong) EColumn *eColumnSelected;
@property (nonatomic, strong) UIColor *tempColor;
@end
@implementation EColumView
@synthesize tempColor = _tempColor;
@synthesize eFloatBox = _eFloatBox;
@synthesize eColumnChart = _eColumnChart;
@synthesize data = _data;
@synthesize eColumnSelected = _eColumnSelected;
@synthesize valueLabel = _valueLabel;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.data = nil;
    
        self.valueArrays = @[@"123",@"412",@"23",@"345",@"812",@"23",@"123"];
    
        self.backgroundColor = [UIColor whiteColor];
        NSLog(@"the new array is %@",self.valueArrays);
        //   这里是柱状图显示多少高度的数据源
        NSMutableArray *temp = [NSMutableArray array];
        for (int i = 0; i < self.valueArrays.count; i++)
        {
            //        int value = arc4random() % 100;
            int value = [NSString stringWithFormat:@"%@",self.valueArrays[i]].intValue;
            NSLog(@"the arc4random number is %d",value);
            EColumnDataModel *eColumnDataModel = [[EColumnDataModel alloc] initWithLabel:[NSString stringWithFormat:@"%d", i] value:value index:i unit:@"次"];
            [temp addObject:eColumnDataModel];
        }
        _data = [NSArray arrayWithArray:temp];
        
        
        //NSArray *array =[[NSArray alloc]initWithArray:self.valueArrays];
        _eColumnChart = [[EColumnChart alloc] initWithFrame:CGRectMake(0, 0, 800, 300)];
        
        _eColumnChart.textArr = @[@"12-01",@"12-02",@"12-03",@"12-04",@"12-05",@"12-06",@"12-07"];
        [_eColumnChart setColumnsIndexStartFromLeft:YES];
        [_eColumnChart setDelegate:self];
        [_eColumnChart setDataSource:self];
        
        
        
        /**
         **************************************************
         */
        
        
        _columnValueLabel = [[UILabel alloc]initWithFrame:CGRectMake(590, 400, 200, 60)];
        _columnValueLabel.textColor = [UIColor blackColor];
        _columnValueLabel.textAlignment = NSTextAlignmentCenter;
        _columnValueLabel.text = @"Welcome! Column!";
        _columnValueLabel.font = [UIFont systemFontOfSize:16.0f];
        
        [self addSubview:_eColumnChart];
    }
    return self;
}


#pragma -mark- Actions
- (IBAction)highlightMaxAndMinChanged:(id)sender
{
    UISwitch *mySwith = (UISwitch *)sender;
    if ([mySwith isOn])
    {
        [_eColumnChart setShowHighAndLowColumnWithColor:YES];
    }
    else
    {
        [_eColumnChart setShowHighAndLowColumnWithColor:NO];
    }
}


- (IBAction)eventHandleChanged:(id)sender
{
    UISwitch *mySwith = (UISwitch *)sender;
    if ([mySwith isOn])
    {
        [_eColumnChart setDelegate:self];
    }
    else
    {
        [_eColumnChart setDelegate:nil];
    }
}

- (IBAction)chartDirectionChanged:(id)sender
{
    UISwitch *mySwith = (UISwitch *)sender;
    if ([mySwith isOn])
    {
        [_eColumnChart removeFromSuperview];
        _eColumnChart = nil;
        _eColumnChart = [[EColumnChart alloc] initWithFrame:CGRectMake(40, 100, 250, 200)];
        [_eColumnChart setColumnsIndexStartFromLeft:YES];
        [_eColumnChart setDelegate:self];
        [_eColumnChart setDataSource:self];
        [self addSubview:_eColumnChart];
    }
    else
    {
        [_eColumnChart removeFromSuperview];
        _eColumnChart = nil;
        _eColumnChart = [[EColumnChart alloc] initWithFrame:CGRectMake(40, 100, 250, 200)];
        [_eColumnChart setColumnsIndexStartFromLeft:NO];
        [_eColumnChart setDelegate:self];
        [_eColumnChart setDataSource:self];
        [self addSubview:_eColumnChart];
    }
}

- (IBAction)leftButtonPressed:(id)sender
{
    if (self.eColumnChart == nil) return;
    [self.eColumnChart moveLeft];
}

- (IBAction)rightButtonPressed:(id)sender
{
    if (self.eColumnChart == nil) return;
    [self.eColumnChart moveRight];
}


#pragma -mark- EColumnChartDataSource

- (NSInteger)numberOfColumnsInEColumnChart:(EColumnChart *)eColumnChart
{
    return [_data count];
}

- (NSInteger)numberOfColumnsPresentedEveryTime:(EColumnChart *)eColumnChart
{
    return self.valueArrays.count;
}

- (EColumnDataModel *)highestValueEColumnChart:(EColumnChart *)eColumnChart
{
    EColumnDataModel *maxDataModel = nil;
    float maxValue = -FLT_MIN;
    for (EColumnDataModel *dataModel in _data)
    {
        if (dataModel.value > maxValue)
        {
            maxValue = dataModel.value;
            maxDataModel = dataModel;
        }
        
        /**
         **********************************************************************
         */
        
        if (dataModel.value < 50) {
            
        }else if (dataModel.value >50&&dataModel.value<100){
            eColumnChart.normalColumnColor = [UIColor orangeColor];
        }else if (dataModel.value >=100){
            eColumnChart.normalColumnColor = [UIColor redColor];
        }
        
        /**
         **********************************************************************
         */
        
    }
    return maxDataModel;
}

- (EColumnDataModel *)eColumnChart:(EColumnChart *)eColumnChart valueForIndex:(NSInteger)index
{
    if (index >= [_data count] || index < 0) return nil;
    return [_data objectAtIndex:index];
}

#pragma -mark- EColumnChartDelegate
- (void)eColumnChart:(EColumnChart *)eColumnChart
     didSelectColumn:(EColumn *)eColumn
{
    NSLog(@"Index: %ld  Value: %f", (long)eColumn.eColumnDataModel.index, eColumn.eColumnDataModel.value);
    self.columnValueLabel.text = [NSString stringWithFormat:@"%f",eColumn.eColumnDataModel.value];
    if (_eColumnSelected)
    {
        _eColumnSelected.barColor = _tempColor;
    }
    _eColumnSelected = eColumn;
    _tempColor = eColumn.barColor;
    eColumn.barColor = [UIColor blackColor];
    
    _valueLabel.text = [NSString stringWithFormat:@"%.0f",eColumn.eColumnDataModel.value];
}

- (void)eColumnChart:(EColumnChart *)eColumnChart
fingerDidEnterColumn:(EColumn *)eColumn
{
    /**The EFloatBox here, is just to show an example of
     taking adventage of the event handling system of the Echart.
     You can do even better effects here, according to your needs.*/
    NSLog(@"Finger did enter %ld", (long)eColumn.eColumnDataModel.index);
    CGFloat eFloatBoxX = eColumn.frame.origin.x + eColumn.frame.size.width * 1.25;
    CGFloat eFloatBoxY = eColumn.frame.origin.y + eColumn.frame.size.height * (1-eColumn.grade);
    if (_eFloatBox)
    {
        [_eFloatBox removeFromSuperview];
        _eFloatBox.frame = CGRectMake(eFloatBoxX, eFloatBoxY, _eFloatBox.frame.size.width, _eFloatBox.frame.size.height);
        [_eFloatBox setValue:eColumn.eColumnDataModel.value];
        [eColumnChart addSubview:_eFloatBox];
    }
    else
    {
        _eFloatBox = [[EFloatBox alloc] initWithPosition:CGPointMake(eFloatBoxX, eFloatBoxY) value:eColumn.eColumnDataModel.value unit:@"次" title:@"访问量"];
        _eFloatBox.alpha = 0.0;
        _eFloatBox.font = [UIFont systemFontOfSize:14.0f];
        [eColumnChart addSubview:_eFloatBox];
        
    }
    eFloatBoxY -= (_eFloatBox.frame.size.height + eColumn.frame.size.width * 0.25);
    _eFloatBox.frame = CGRectMake(eFloatBoxX, eFloatBoxY, _eFloatBox.frame.size.width, _eFloatBox.frame.size.height);
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionTransitionNone animations:^{
        _eFloatBox.alpha = 1.0;
        
    } completion:^(BOOL finished) {
    }];
    
}

- (void)eColumnChart:(EColumnChart *)eColumnChart
fingerDidLeaveColumn:(EColumn *)eColumn
{
    NSLog(@"Finger did leave %ld", (long)eColumn.eColumnDataModel.index);
    
}

- (void)fingerDidLeaveEColumnChart:(EColumnChart *)eColumnChart
{
    if (_eFloatBox)
    {
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionTransitionNone animations:^{
            _eFloatBox.alpha = 0.0;
            _eFloatBox.frame = CGRectMake(_eFloatBox.frame.origin.x, _eFloatBox.frame.origin.y + _eFloatBox.frame.size.height, _eFloatBox.frame.size.width, _eFloatBox.frame.size.height);
        } completion:^(BOOL finished) {
            [_eFloatBox removeFromSuperview];
            _eFloatBox = nil;
        }];
        
    }
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
