//
//  EColumnChartLabel.h
//  EChart
//
//  Created by Efergy China on 13/12/13.
//  Copyright (c) 2013 Scott Zhu. All rights reserved.
//


//这个类使用来控制柱状图中的数据的lable的显示

#import <UIKit/UIKit.h>

@interface EColumnChartLabel : UILabel

@end
