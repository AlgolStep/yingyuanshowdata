//
//  EColumView.h
//  XYPieChart
//
//  Created by 思来氏 on 14-9-25.
//  Copyright (c) 2014年 Xiaoyang Feng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EColumnChart.h"

@interface EColumView : UIView<EColumnChartDelegate, EColumnChartDataSource>
@property (strong, nonatomic) EColumnChart *eColumnChart;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (nonatomic, strong)UILabel *columnValueLabel;
@property (nonatomic, strong)NSArray *valueArrays;
@end
