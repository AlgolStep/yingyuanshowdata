//
//  EColor.h
//  EChart
//
//  Created by Efergy China on 12/12/13.
//  Copyright (c) 2013 Scott Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>


#define EGrey         [UIColor colorWithRed:207.0/255.0 green:224.0/255.0 blue:113.0/255.0 alpha:1.0f]
#define ELightBlue    [UIColor colorWithRed:94.0/255.0 green:147.0/255.0 blue:196.0/255.0 alpha:1.0f]
#define EGreen        [UIColor colorWithRed:138.0/255.0 green:192.0/255.0 blue:66.0/255.0 alpha:1.0f]
#define EDeepGrey     [UIColor colorWithRed:99.0/255.0 green:99.0/255.0 blue:99.0/255.0 alpha:1.0f]
#define ELightGrey    [UIColor colorWithRed:225.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1.0f]
#define EBlack        [UIColor colorWithRed:45.0/255.0 green:45.0/255.0 blue:45.0/255.0 alpha:1.0f]

#define EMaxValueColor   [UIColor colorWithRed:246.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0]
#define EMinValueColor   [UIColor colorWithRed:133.0/255.0 green:218.0/255.0 blue:233.0/255.0 alpha:1.0]
#define EBlueGreenColor  [UIColor colorWithRed:207.0/255.0 green:224.0/255.0 blue:113.0/255.0 alpha:0.5]


#define EYellow [UIColor colorWithRed:255.0/255.0 green:227.0/255.0 blue:61.0/255.0 alpha:1]
#define EBlue [UIColor colorWithRed:100.0/255.0 green:236.0/255.0 blue:241.0/255.0 alpha:1]
@interface EColor : NSObject
@end
