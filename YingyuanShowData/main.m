//
//  main.m
//  YingyuanShowData
//
//  Created by 思来氏 on 14-11-24.
//  Copyright (c) 2014年 PSYDemo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
