//
//  YYVideoPlayViewController.m
//  YingYuanMath
//
//  Created by Psylife_iPro02  on 14-10-23.
//  Copyright (c) 2014年 psylife. All rights reserved.
//

#import "YYVideoPlayViewController.h"
#import "CustomButton.h"
#import "SubDemoTableViewController.h"

@interface YYVideoPlayViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataArr;
    
    NSArray *titleArr;
    NSArray *detailContentArr;
    NSArray *starLevelArr;

}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation YYVideoPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.allowsSelection = NO;
    dataArr = @[@"video1",@"video2",@"video3",@"video4"];
    titleArr = [[NSArray alloc]initWithObjects:@"完美的约会对象",@"Frank的驾车烦恼",@"Pancake的比值表",@"逗趣的数学猫",nil];
    detailContentArr = [[NSArray alloc]initWithObjects:@"完美的约会对象应该是说话滔滔不绝呢？还是惜字如金呢？用比和比例算一下就知道……",@"Frank要从圣地亚哥驱车前往旧金山见一位朋友，他只知道两地相距离500英里，却不知道车程需要多久……",@"图表和动画，一步一步帮你理解比的意义和性质……",@"数学猫用8个有趣的生活场景教你学习比的知识….",nil];
    starLevelArr = [[NSArray alloc]initWithObjects:@"听力难度：两颗星",@"听力难度：三颗星",@"听力难度：一颗星",@"听力难度：一颗星",nil];
}
#pragma mark --UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (dataArr.count != 0) {
        return [dataArr count];
    }
    return 0;

}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *indentifier = @"VideoCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if (cell == nil) {
        
        cell = [[[NSBundle mainBundle]loadNibNamed:@"videoCell" owner:self options:nil]lastObject];

    }
    
    //添加播放按钮
    UIButton *btn = (id)[cell viewWithTag:1110];
    CustomButton *videoButton =[CustomButton buttonWithType:UIButtonTypeCustom];
    videoButton.frame = CGRectMake(btn.frame.origin.x, btn.frame.origin.y, btn.frame.size.width, btn.frame.size.height);
    videoButton.indexPath = indexPath;
    [videoButton addTarget:self action:@selector(videoButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:videoButton];
    
    //标签说明
    UILabel *label1 = (UILabel*)[cell viewWithTag:1111];
    if ([titleArr count]!=0) {
        label1.text = [titleArr objectAtIndex:indexPath.row];
    }
    
    UILabel *label2 = (UILabel*)[cell viewWithTag:1112];
    if ([detailContentArr count]!=0) {
        label2.text = [detailContentArr objectAtIndex:indexPath.row];
    }
    
    UILabel *label3 = (UILabel*)[cell viewWithTag:1113];
    if ([starLevelArr count]!=0) {
        label3.text = [starLevelArr objectAtIndex:indexPath.row];
    }
    //滑动条
    UISlider *slider = (UISlider*)[cell viewWithTag:1114];
    slider.tag = 1000+indexPath.row;
    [slider addTarget:self action:@selector(sliderChangeValue:) forControlEvents:UIControlEventValueChanged];
    
    
    return cell;

}
#pragma mark --UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 200.f;

}



#pragma mark -- Event Method
-(void)videoButtonClick:(UIButton*)sender{
    
    NSLog(@"sender.tag =%d",sender.tag);
    
    if (pop != nil) {
        [pop dismissPopoverAnimated:NO];
    }
    SubDemoTableViewController *demoVC = [[SubDemoTableViewController alloc]initWithStyle:UITableViewStylePlain];
    //    demoVC.btnTag = sender.tag;//标题
    //demoVC.delegate = self;
    pop = [[FPPopoverController alloc]initWithViewController:demoVC];
    pop.tint = FPPopoverRedTint;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        pop.contentSize = CGSizeMake(360, 280);
    }else{
        pop.contentSize = CGSizeMake(200, 300);
        
    }
    pop.arrowDirection = FPPopoverArrowDirectionAny;
    NSLog(@"sender.tag = %d",sender.tag);
    
    [pop presentPopoverFromView:sender];

    
    
    
    
    
    
}
-(void)sliderChangeValue:(UISlider*)slider{
    
}

- (IBAction)dimissButtonClick:(UIButton *)sender {
   
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
