//
//  TitleViewController.m
//  YingyuanShowData
//
//  Created by 思来氏 on 14-11-25.
//  Copyright (c) 2014年 PSYDemo. All rights reserved.
//

#import "TitleViewController.h"
#import "StudentViewController.h"
#import "DataViewController.h"
#import "MainViewController.h"

@interface TitleViewController ()
@property (strong, nonatomic) IBOutlet UIButton *showViewButton;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation TitleViewController
{
    CGRect originViewFrame;
    NSString *contentString;
    costumView *ccView;
    
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    NSString *path=[[NSBundle mainBundle] pathForResource:@"questionsName" ofType:@"plist"];
    self.dataDictionary=[NSDictionary dictionaryWithContentsOfFile:path];
    self.pdfNameArray = [self.dataDictionary objectForKey:@"pdfName"];
    self.questionsNameArray = [self.dataDictionary objectForKey:@"questionsName"];
    
    ccView = [[costumView alloc]initWithFrame:CGRectMake(0, -48, 1024, 48)];
    ccView.delegate = self;
    originViewFrame = ccView.frame;
    [ccView.buttonC setImage:[UIImage imageNamed:@"触发按题目"] forState:UIControlStateNormal];
    [self.view addSubview:ccView];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
     
    
    self.slicesArray = @[@"12",@"23",@"34",@"54",@"21"];
    self.slicesNameArray = @[@"版块一",@"版块二",@"版块三",@"版块四",@"版块五"];
    
    [self.pieChartView setDataSource:self];
    [self.pieChartView setStartPieAngle:M_PI_2];
    [self.pieChartView setAnimationSpeed:1.0];
    [self.pieChartView setLabelFont:[UIFont fontWithName:@"DBLCDTempBlack" size:24]];
    [self.pieChartView setLabelRadius:100];
    [self.pieChartView setShowPercentage:YES];
    [self.pieChartView setPieBackgroundColor:[UIColor colorWithWhite:1.0 alpha:1]];
    [self.pieChartView setPieCenter:CGPointMake(190, 190)];
    [self.pieChartView setUserInteractionEnabled:YES];
    self.percentageLabel.layer.cornerRadius = 50.0f;
    
    
    [self.pieChartRightView setDataSource:self];
    [self.pieChartRightView setStartPieAngle:M_PI_2];
    [self.pieChartRightView setAnimationSpeed:1.0];
    [self.pieChartRightView setLabelFont:[UIFont fontWithName:@"DBLCDTempBlack" size:24]];
    [self.pieChartRightView setLabelRadius:100];
    [self.pieChartRightView setShowPercentage:YES];
    [self.pieChartRightView setPieBackgroundColor:[UIColor colorWithWhite:1.0 alpha:1]];
    [self.pieChartRightView setPieCenter:CGPointMake(190, 190)];
    [self.pieChartRightView setUserInteractionEnabled:YES];
    self.pieChartRightView.tag = 10001;
    self.percentageRightLable.layer.cornerRadius = 50.0f;
    
    self.sliceColors =[NSArray arrayWithObjects:
                       [UIColor colorWithRed:246/255.0 green:155/255.0 blue:0/255.0 alpha:1],
                       [UIColor colorWithRed:129/255.0 green:195/255.0 blue:29/255.0 alpha:1],
                       [UIColor colorWithRed:62/255.0 green:173/255.0 blue:219/255.0 alpha:1],
                       [UIColor colorWithRed:229/255.0 green:66/255.0 blue:115/255.0 alpha:1],
                       [UIColor colorWithRed:148/255.0 green:141/255.0 blue:139/255.0 alpha:1],nil];
    
    self.sliceRightColors =[NSArray arrayWithObjects:
                       [UIColor colorWithRed:142/255.0 green:152/255.0 blue:255/255.0 alpha:1],
                       [UIColor colorWithRed:125/255.0 green:45/255.0 blue:50/255.0 alpha:1],
                       [UIColor colorWithRed:125/255.0 green:176/255.0 blue:50/255.0 alpha:1],
                       [UIColor colorWithRed:229/255.0 green:66/255.0 blue:115/255.0 alpha:1],
                       [UIColor colorWithRed:148/255.0 green:141/255.0 blue:139/255.0 alpha:1],nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setWebViewDoc:self.pdfNameArray[0]];
    self.percentageLabel.text = @"%";
    [self.pieChartView reloadData];
    [self.pieChartRightView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.questionsNameArray.count;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if ( nil == cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = self.questionsNameArray[indexPath.row];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont systemFontOfSize:20.0f];
    return cell;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self setWebViewDoc:self.pdfNameArray[indexPath.row]];
    self.pieChartView.hidden = NO;
    self.webView.hidden = NO;
    [self.pieChartView reloadData];
    [self.pieChartView reloadData];
    
}


#pragma mark - XYPieChart Data Source

- (NSUInteger)numberOfSlicesInPieChart:(PSYPieChart *)pieChart
{
    return self.slicesArray.count;
}

- (CGFloat)pieChart:(PSYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    
    return [[self.slicesArray objectAtIndex:index] intValue];
    
}

- (UIColor *)pieChart:(PSYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    if (pieChart.tag == 10001) {
        return [self.sliceRightColors objectAtIndex:(index % self.sliceColors.count)];
    }else{
        return [self.sliceColors objectAtIndex:(index % self.sliceColors.count)];
    }
    
}

- (NSArray*)pieChartNameArray:(PSYPieChart *)pieChart
{
    return self.slicesNameArray;
}


#pragma mark - XYPieChart Delegate
- (void)pieChart:(PSYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
{
    NSLog(@"did select slice at index %d",index);
}




- (IBAction)onShowViewClick:(id)sender {
    [self showDataView];
    self.showViewButton.hidden = YES;
}

#pragma mark - ButtonChangeViewController Delegate

- (void)changeToCircleViewController
{
    MainViewController *mainViewController = [[MainViewController alloc]initWithNibName:@"MainViewController" bundle:nil];
    [self presentViewController:mainViewController animated:YES completion:nil];
}
- (void)changeToStudentViewController
{
    StudentViewController *studentViewController = [[StudentViewController alloc]initWithNibName:@"StudentViewController" bundle:nil];
    [self presentViewController:studentViewController animated:YES completion:nil];
}
- (void)changeToTitleViewController
{
//    TitleViewController *titleViewController = [[TitleViewController alloc]initWithNibName:@"TitleViewController" bundle:nil];
//    [self presentViewController:titleViewController animated:YES completion:nil];
}
- (void)changeToDataViewController
{
    DataViewController *dataViewController = [[DataViewController alloc]initWithNibName:@"DataViewController" bundle:nil];
    [self presentViewController:dataViewController animated:YES completion:nil];
}

- (void)hideView
{
    [self hideDataView];
    self.showViewButton.hidden = NO;
}

//在web视图上展示问题的具体内容
-(void) setWebViewDoc:(NSString*)pdfNameStr
{
    NSString *docPath = [[NSBundle mainBundle] pathForResource:pdfNameStr ofType:@"pdf"];
    NSURL *url = [NSURL fileURLWithPath:docPath];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

//显现导航视图
- (void)showDataView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    CGRect newFrame = CGRectMake(ccView.frame.origin.x, 0, ccView.frame.size.width, ccView.frame.size.height);
    ccView.frame = newFrame;
    ccView.hidden = NO;
    [UIView commitAnimations];
}

- (void)hideDataView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    ccView.frame = originViewFrame;
    //    self.showView.hidden = YES;
    [UIView commitAnimations];
}

@end
