//
//  costumView.m
//  YingyuanShowData
//
//  Created by 思来氏 on 14-12-5.
//  Copyright (c) 2014年 PSYDemo. All rights reserved.
//

#import "costumView.h"


@implementation costumView
{
    CGRect originFrame;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 1024, 48)];
        [imageView setImage:[UIImage imageNamed:@"底图.png"]];
        
        
        UIButton *hidenViewButton = [[UIButton alloc]initWithFrame:CGRectMake(8, 8, 32, 30)];
        [hidenViewButton addTarget:self action:@selector(onHidenViewClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _buttonA = [[UIButton alloc]initWithFrame:CGRectMake(80, 6, 93, 37)];
        [_buttonA setImage:[UIImage imageNamed:@"选项按环节"] forState:UIControlStateNormal];
        [_buttonA addTarget:self action:@selector(onButtonAClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _buttonB = [[UIButton alloc]initWithFrame:CGRectMake(210, 6, 93, 37)];
        [_buttonB setImage:[UIImage imageNamed:@"选项按学生"] forState:UIControlStateNormal];
        [_buttonB addTarget:self action:@selector(onButtonBClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _buttonC = [[UIButton alloc]initWithFrame:CGRectMake(340, 6, 93, 37)];
        [_buttonC setImage:[UIImage imageNamed:@"选项按题目"] forState:UIControlStateNormal];
        [_buttonC addTarget:self action:@selector(onButtonCClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _buttonD = [[UIButton alloc]initWithFrame:CGRectMake(466, 6, 93, 37)];
        [_buttonD setImage:[UIImage imageNamed:@"选项按数据"] forState:UIControlStateNormal];
        [_buttonD addTarget:self action:@selector(onButtonDClick:) forControlEvents:UIControlEventTouchUpInside];
        originFrame = frame;
        [self addSubview:imageView];
        [self addSubview:hidenViewButton];
        [self addSubview:_buttonA];
        [self addSubview:_buttonB];
        [self addSubview:_buttonC];
        [self addSubview:_buttonD];
    }
    return self;
}


- (void)onHidenViewClick:(UIButton*)sender
{
    [self.delegate hideView];
}

- (void)onButtonAClick:(UIButton*)sender
{
    [self.delegate changeToCircleViewController];
}

- (void)onButtonBClick:(UIButton*)sender
{
    [self.delegate changeToStudentViewController];
}

- (void)onButtonCClick:(UIButton*)sender
{
    [self.delegate changeToTitleViewController];
}

- (void)onButtonDClick:(UIButton*)sender
{
//    [self.delegate changeToDataViewController];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


@end
