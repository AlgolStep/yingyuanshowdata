//
//  costumView.h
//  YingyuanShowData
//
//  Created by 思来氏 on 14-12-5.
//  Copyright (c) 2014年 PSYDemo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ButtonChangeViewControllerDelegate <NSObject>

- (void)changeToCircleViewController;
- (void)changeToStudentViewController;
- (void)changeToTitleViewController;
- (void)changeToDataViewController;
- (void)hideView;
@end
@interface costumView : UIView
@property (nonatomic, weak) id <ButtonChangeViewControllerDelegate> delegate;
@property (nonatomic, strong) UIButton *buttonA;
@property (nonatomic, strong) UIButton *buttonB;
@property (nonatomic, strong) UIButton *buttonC;
@property (nonatomic, strong) UIButton *buttonD;
@end
