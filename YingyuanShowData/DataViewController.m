//
//  DataViewController.m
//  YingyuanShowData
//
//  Created by 思来氏 on 14-11-25.
//  Copyright (c) 2014年 PSYDemo. All rights reserved.
//

#import "DataViewController.h"
#import "MainViewController.h"
#import "TitleViewController.h"
#import "StudentViewController.h"

@interface DataViewController ()
@property (strong, nonatomic) IBOutlet UIButton *showViewButton;

@end

@implementation DataViewController
{
    CGRect originViewFrame;
    costumView *ccView;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    ccView = [[costumView alloc]initWithFrame:CGRectMake(0, -48, 1024, 48)];
    ccView.delegate = self;
    originViewFrame = ccView.frame;
    [ccView.buttonD setImage:[UIImage imageNamed:@"触发按数据"] forState:UIControlStateNormal];
    [self.view addSubview:ccView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onShowView:(id)sender {
    [self showDataView];
    self.showViewButton.hidden = YES;
}



- (void)showDataView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    CGRect newFrame = CGRectMake(ccView.frame.origin.x, 0, ccView.frame.size.width, ccView.frame.size.height);
    ccView.frame = newFrame;
    ccView.hidden = NO;
    [UIView commitAnimations];
}

- (void)hideDataView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    ccView.frame = originViewFrame;
    //    self.showView.hidden = YES;
    [UIView commitAnimations];
}

#pragma mark - ButtonChangeViewController Delegate

- (void)changeToCircleViewController
{
    MainViewController *mainViewController = [[MainViewController alloc]initWithNibName:@"MainViewController" bundle:nil];
    [self presentViewController:mainViewController animated:YES completion:nil];
}
- (void)changeToStudentViewController
{
    StudentViewController *studentViewController = [[StudentViewController alloc]initWithNibName:@"StudentViewController" bundle:nil];
    [self presentViewController:studentViewController animated:YES completion:nil];
}
- (void)changeToTitleViewController
{
    TitleViewController *titleViewController = [[TitleViewController alloc]initWithNibName:@"TitleViewController" bundle:nil];
    [self presentViewController:titleViewController animated:YES completion:nil];
}
- (void)changeToDataViewController
{
    
}

-(void)hideView
{
    [self hideDataView];
    self.showViewButton.hidden = NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
