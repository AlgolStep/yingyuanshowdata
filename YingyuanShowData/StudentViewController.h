//
//  StudentViewController.h
//  YingyuanShowData
//
//  Created by 思来氏 on 14-11-25.
//  Copyright (c) 2014年 PSYDemo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPGraphView.h"
#import "MPPlot.h"
#import "MPBarsGraphView.h"
#import "PSYPieChart.h"
#import "costumView.h"

@interface StudentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,XYPieChartDataSource,XYPieChartDelegate,ButtonChangeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet PSYPieChart *pieChartView;
@property (strong, nonatomic) IBOutlet PSYPieChart *pieChartRightView;
@property (strong, nonatomic) IBOutlet UILabel *percentageRightLable;
@property (strong, nonatomic) IBOutlet UILabel *percentageLabel;

@property (strong, nonatomic) NSArray *sliceColors;
@property (strong, nonatomic) NSArray *sliceRightColors;
@property (strong, nonatomic) NSArray *slicesArray;
@property (strong, nonatomic) NSArray *slicesNameArray;
@property (strong, nonatomic) NSArray *slicesRightArray;
@property (strong, nonatomic) NSArray *slicesRightNameArray;

@property (strong, nonatomic) NSDictionary *infoDictionary;
@property (strong, nonatomic) NSDictionary *teacherDictionary;
@property (strong, nonatomic) NSArray *classArray;
@property (strong, nonatomic) NSArray *studentsNameArray;
@property (strong, nonatomic) NSArray *studentDataArray;
@property (strong, nonatomic) NSDictionary *studentsDictionary;
@property (strong, nonatomic) NSDictionary *masterDictionary;
@property (strong, nonatomic) NSDictionary *exploreDictionary;
@end
