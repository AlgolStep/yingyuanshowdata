//
//  DetailViewController.m
//  chart2221
//
//  Created by 思来氏 on 14-10-22.
//  Copyright (c) 2014年 com.ELS. All rights reserved.
//

#import "DetailViewController.h"

#import "ASFTableView.h"

//#import "SHLineGraphView.h"
//#import "SHPlot.h"
@interface DetailViewController ()<ASFTableViewDelegate>
{
    UITableView *detailTableView;
}

@property (nonatomic, retain) NSMutableArray *rowsArray;

@end

@implementation DetailViewController

//把UIView添加到你的视图里，并把父类设为ASFTableView
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableViewData:) name:@"refreshTableViewData" object:nil];
    
//    self.contentArray =@[@"student ID",@"student Name",@" Class No.",@"80",@"90",@"100",];
    self.title = @"详情窗口";
    self.view.backgroundColor = [UIColor whiteColor];
    //大表格
    _mASFTableView  = [[ASFTableView alloc] initWithFrame:CGRectMake(0, 0, 800, 328)];
    
    _rowsArray = [[NSMutableArray alloc] init];
    NSArray *cols = @[@"时     间",@"类      别1",@"类      别2",@"类      别3",@"内      容",@"时      长"];
    NSArray *weights = @[@(0.20f),@(0.15f),@(0.15f),@(0.15f),@(0.18f),@(0.17f),@(0.15f)];
    //表section设置
    NSDictionary *options = @{kASF_OPTION_CELL_TEXT_FONT_SIZE : @(16),   //字体
                              kASF_OPTION_CELL_TEXT_FONT_BOLD : @(true), // 黑体
                              kASF_OPTION_CELL_TEXT_COLOR:[UIColor whiteColor],
                              kASF_OPTION_CELL_BORDER_COLOR : [UIColor whiteColor], //边界线颜色
                              kASF_OPTION_CELL_BORDER_SIZE : @(1.0),  //边界大小
                              kASF_OPTION_BACKGROUND : [UIColor colorWithRed:68/255.0 green:128/255.0 blue:150/255.0 alpha:1.0]};// section头颜色
    // 表格样式设置
    [_mASFTableView setDelegate:self];
    [_mASFTableView setBounces:YES];
    [_mASFTableView setSelectionColor:[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0f]]; //选中单元格颜色
    [_mASFTableView setTitles:cols
                  WithWeights:weights
                  WithOptions:options
                    WitHeight:32 Floating:YES];
    [_mASFTableView setBackgroundColor:[UIColor whiteColor]];  //背景色
    
    [_rowsArray removeAllObjects];
    //表格内容设置
    int sumNumber;
    if (self.contentArray.count>2) {
        sumNumber = self.contentArray.count - 2;
    }else{
        sumNumber = 0;
    }
    for (int i=0; i<sumNumber; i++) {
        NSDictionary *tempDictionary = self.contentArray[i];
        [_rowsArray addObject:@{
                                kASF_ROW_ID :           //ID
                                @(i),
                                kASF_ROW_CELLS :        // Cells
                                @[@{kASF_CELL_TITLE : [tempDictionary objectForKey:@"shijiancuo"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)},
                                  @{kASF_CELL_TITLE : [tempDictionary objectForKey:@"leibie1"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentLeft)},
                                  @{kASF_CELL_TITLE : [tempDictionary objectForKey:@"leibie2"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)},
                                  @{kASF_CELL_TITLE : [tempDictionary objectForKey:@"leibie3"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)},
                                  @{kASF_CELL_TITLE : [tempDictionary objectForKey:@"neirong"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)},
                                  @{kASF_CELL_TITLE : [tempDictionary objectForKey:@"shichang"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)},
                                  ],
//                                @{kASF_CELL_TITLE : self.contentArray[6], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)}
                                kASF_ROW_OPTIONS :          // Options
                                @{kASF_OPTION_BACKGROUND : [UIColor clearColor],
                                  kASF_OPTION_CELL_BACKGROUND:[UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.0],
                                  kASF_OPTION_CELL_TEXT_COLOR:[UIColor colorWithRed:68/255.0 green:128/255.0 blue:150/255.0 alpha:1.0],
                                  kASF_OPTION_CELL_PADDING : @(5),
                                  kASF_OPTION_CELL_BORDER_COLOR : [UIColor whiteColor],
                                  kASF_OPTION_CELL_BORDER_SIZE:@(1.0)},
                                @"some_other_data" : @(123)}];
    }
    
    [_mASFTableView setRows:_rowsArray];
    [self.view addSubview:_mASFTableView];
    
    //折线图
    
//    SHLineGraphView *_lineGraph = [[SHLineGraphView alloc] initWithFrame:CGRectMake(0, 50, 704, 300)];
    //set the main graph area theme attributes
    
    /**
     *  theme attributes dictionary. you can specify graph theme releated attributes in this dictionary. if this property is
     *  nil, then a default theme setting is applied to the graph.
     */
//    NSDictionary *_themeAttributes = @{
//                                       kXAxisLabelColorKey : [UIColor colorWithRed:0.48 green:0.48 blue:0.49 alpha:0.4],
//                                       kXAxisLabelFontKey : [UIFont fontWithName:@"TrebuchetMS" size:10],
//                                       kYAxisLabelColorKey : [UIColor colorWithRed:0.48 green:0.48 blue:0.49 alpha:0.4],
//                                       kYAxisLabelFontKey : [UIFont fontWithName:@"TrebuchetMS" size:10],
//                                       kYAxisLabelSideMarginsKey : @20,
//                                       kPlotBackgroundLineColorKye : [UIColor colorWithRed:0.48 green:0.48 blue:0.49 alpha:0.4]
//                                       };
//    NSDictionary *_themeAttributes = @{}; //设置X軸Y軸的字体（淡化颜色，大小） ，横轴的显示
//    _lineGraph.themeAttributes = _themeAttributes;
    
    //set the line graph attributes
    
    /**
     *  the maximum y-value possible in the graph. make sure that the y-value is not in the plotting points is not greater
     *  then this number. otherwise the graph plotting will show wrong results.
     */
//    _lineGraph.yAxisRange = @(100); // Y 轴最大值为100
    
    /**
     *  y-axis values are calculated according to the yAxisRange passed. so you do not have to pass the explicit labels for
     *  y-axis, but if you want to put any suffix to the calculated y-values, you can mention it here (e.g. K, M, Kg ...)
     */
//    _lineGraph.yAxisSuffix = @"K"; //   Y 轴单位
    
    /**
     *  an Array of dictionaries specifying the key/value pair where key is the object which will identify a particular
     *  x point on the x-axis line. and the value is the label which you want to show on x-axis against that point on x-axis.
     *  the keys are important here as when plotting the actual points on the graph, you will have to use the same key to
     *  specify the point value for that x-axis point.
//     */
//    _lineGraph.xAxisValues = @[
//                               @{ @1 : @"JAN" },
//                               @{ @2 : @"FEB" },
//                               @{ @3 : @"MAR" },
//                               @{ @4 : @"APR" },
//                               @{ @5 : @"MAY" },
//                               @{ @6 : @"JUN" },
//                               @{ @7 : @"JUL" },
//                               @{ @8 : @"AUG" },
//                               @{ @9 : @"SEP" },
//                               @{ @10 : @"OCT" },
//                               @{ @11 : @"NOV" },
//                               @{ @12 : @"DEC" }
//                               ];
    //  X 轴则是  key/value
    
    //create a new plot object that you want to draw on the `_lineGraph`
//    SHPlot *_plot1 = [[SHPlot alloc] init];
    
    //set the plot attributes
    
    /**
     *  Array of dictionaries, where the key is the same as the one which you specified in the `xAxisValues` in `SHLineGraphView`,
     *  the value is the number which will determine the point location along the y-axis line. make sure the values are not
     *  greater than the `yAxisRange` specified in `SHLineGraphView`.
     */
//    _plot1.plottingValues = @[
//                              @{ @1 : @65.8 },
//                              @{ @2 : @20 },
//                              @{ @3 : @23 },
//                              @{ @4 : @22 },
//                              @{ @5 : @12.3 },
//                              @{ @6 : @45.8 },
//                              @{ @7 : @56 },
//                              @{ @8 : @97 },
//                              @{ @9 : @65 },
//                              @{ @10 : @10 },
//                              @{ @11 : @67 },
//                              @{ @12 : @23 }
//                              ];
    
    /**
     *  this is an optional array of `NSString` that specifies the labels to show on the particular points. when user clicks on
     *  a particular points, a popover view is shown and will show the particular label on for that point, that is specified
     *  in this array.
     */
//    NSArray *arr = @[@"1111", @"2", @"3", @"4", @"5", @"6" , @"7" , @"8", @"9", @"10", @"11", @"12"];
//    _plot1.plottingPointsLabels = arr;  // 自定义View上label上显示的
  
    
    //set plot theme attributes
    
    /**
     *  the dictionary which you can use to assing the theme attributes of the plot. if this property is nil, a default theme
     *  is applied selected and the graph is plotted with those default settings.
     */
    
//    NSDictionary *_plotThemeAttributes = @{
//                                           kPlotFillColorKey : [UIColor colorWithRed:0.97 green:0.05 blue:0.08 alpha:0.9],// 填充颜色
//                                           kPlotStrokeWidthKey : @4,                                                       //线条宽度
//                                           kPlotStrokeColorKey : [UIColor colorWithRed:0.1 green:0.81 blue:0.1 alpha:1],   //线条颜色
//                                           kPlotPointFillColorKey : [UIColor colorWithRed:0.08 green:0.06 blue:0.91 alpha:1],// 关键点颜色
//                                           kPlotPointValueFontKey : [UIFont fontWithName:@"TrebuchetMS" size:18]
//                                           };
//    _plot1.plotThemeAttributes = _plotThemeAttributes;
//    [_lineGraph addPlot:_plot1];
    
    //You can as much `SHPlots` as you can in a `SHLineGraphView`
    
//    [_lineGraph setupTheView];
    
//    [self.view addSubview:_lineGraph];
//    [self.view bringSubviewToFront:_lineGraph];
}

- (void)refreshTableViewData:(NSNotification*)notification
{
    [_rowsArray removeAllObjects];
    int sumNumber;
    if (self.contentArray.count>2) {
        sumNumber = self.contentArray.count - 2;
    }else{
        sumNumber = 0;
    }
    for (int i=0; i<sumNumber; i++) {
        NSDictionary *tempDictionary = self.contentArray[i];
        [_rowsArray addObject:@{
                                kASF_ROW_ID :           //ID
                                @(i),
                                kASF_ROW_CELLS :        // Cells
                                @[@{kASF_CELL_TITLE : [tempDictionary objectForKey:@"shijiancuo"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)},
                                  @{kASF_CELL_TITLE : [tempDictionary objectForKey:@"leibie1"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentLeft)},
                                  @{kASF_CELL_TITLE : [tempDictionary objectForKey:@"leibie2"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)},
                                  @{kASF_CELL_TITLE : [tempDictionary objectForKey:@"leibie3"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)},
                                  @{kASF_CELL_TITLE : [tempDictionary objectForKey:@"neirong"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)},
                                  @{kASF_CELL_TITLE : [tempDictionary objectForKey:@"shichang"], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)},
                                  ],
                                //                                @{kASF_CELL_TITLE : self.contentArray[6], kASF_OPTION_CELL_TEXT_ALIGNMENT : @(NSTextAlignmentCenter)}
                                kASF_ROW_OPTIONS :          // Options
                                @{kASF_OPTION_BACKGROUND : [UIColor clearColor],
                                  kASF_OPTION_CELL_BACKGROUND:[UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.0],
                                  kASF_OPTION_CELL_TEXT_COLOR:[UIColor colorWithRed:68/255.0 green:128/255.0 blue:150/255.0 alpha:1.0],
                                  kASF_OPTION_CELL_PADDING : @(5),
                                  kASF_OPTION_CELL_BORDER_COLOR : [UIColor whiteColor],
                                  kASF_OPTION_CELL_BORDER_SIZE:@(1.0)},
                                @"some_other_data" : @(123)}];
    }
    
    [_mASFTableView setRows:_rowsArray];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeLeft;
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

    

#pragma mark - ASFTableViewDelegate
- (void)ASFTableView:(ASFTableView *)tableView DidSelectRow:(NSDictionary *)rowDict WithRowIndex:(NSUInteger)rowIndex {
    NSLog(@"%lu", (unsigned long)rowIndex);
    
}
/*
 
 ASFTableViewPublicGlobals.h 头文件里包含以下自定义选项：
 
 kASF_OPTION_BACKGROUND;        头－－》 red
 kASF_OPTION_CELL_BACKGROUND;
 kASF_OPTION_CELL_BORDER_COLOR;
 kASF_OPTION_CELL_BORDER_SIZE;
 kASF_OPTION_CELL_PADDING;
 kASF_OPTION_CELL_TEXT_FONT_SIZE;
 kASF_OPTION_CELL_TEXT_FONT_BOLD;
 kASF_OPTION_CELL_TEXT_COLOR;
 kASF_OPTION_CELL_TEXT_ALIGNMENT;
 
 kASF_ROW_ID;
 kASF_INNER_ROW_WEIGHTS;
 kASF_ROW_CELLS;
 kASF_ROW_OPTIONS;
 
 kASF_IS_INNER_ROW;
 
 kASF_CELL_TITLE;
 kASF_CELL_IS_EDITABLE;
 kASF_CELL_TEXTVIEW_DELEGATE;
 kASF_CELL_TEXTVIEW_TAG;
 kASF_CELL_IS_KEY;

 */
@end
