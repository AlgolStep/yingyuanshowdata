//
//  YYIsKonwViewController.m
//  YingYuanMath
//
//  Created by 赵 峰 on 14-9-19.
//  Copyright (c) 2014年 psylife. All rights reserved.
//

#import "YYIsKonwViewController.h"
#import "SubDemoTableViewController.h"


@interface YYIsKonwViewController (){
FPPopoverController *pop;
}
@property (strong, nonatomic) IBOutlet UIImageView *image1;
@property (strong, nonatomic) IBOutlet UIImageView *image2;
@property (strong, nonatomic) IBOutlet UIImageView *image3;

@property (strong, nonatomic) IBOutlet UIImageView *image4;
@property (strong, nonatomic) IBOutlet UIImageView *image5;
@property (strong, nonatomic) IBOutlet UIImageView *image6;


@end

@implementation YYIsKonwViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)imageForTap
{
    UITapGestureRecognizer* tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SelectisKonow:)];
    UITapGestureRecognizer* tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SelectisKonow:)];
    UITapGestureRecognizer* tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SelectisKonow:)];
    UITapGestureRecognizer* tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SelectisKonow:)];
    UITapGestureRecognizer* tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SelectisKonow:)];
    UITapGestureRecognizer* tap6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SelectisKonow:)];
    [self.image1 addGestureRecognizer:tap1];
    [self.image2 addGestureRecognizer:tap2];
    [self.image3 addGestureRecognizer:tap3];
    [self.image4 addGestureRecognizer:tap4];
    [self.image5 addGestureRecognizer:tap5];
    [self.image6 addGestureRecognizer:tap6];
    self.image1.clipsToBounds = YES;
    self.image1.layer.cornerRadius = 5;
    self.image2.clipsToBounds = YES;
    self.image2.layer.cornerRadius = 5;
    self.image3.clipsToBounds = YES;
    self.image3.layer.cornerRadius = 5;
    self.image4.clipsToBounds = YES;
    self.image4.layer.cornerRadius = 5;
    self.image5.clipsToBounds = YES;
    self.image5.layer.cornerRadius = 5;
    self.image6.clipsToBounds = YES;
    self.image6.layer.cornerRadius = 5;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self performSelectorInBackground:@selector(imageForTap) withObject:self];//在线程中给image赋予点击方法，这样会流畅
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BackSuperView:(id)sender {
  
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (IBAction)btnClick:(UIButton *)sender {
    
    if (pop != nil) {
        [pop dismissPopoverAnimated:NO];
    }
    
    SubDemoTableViewController *demoVC = [[SubDemoTableViewController alloc]initWithStyle:UITableViewStylePlain];
    
    
    pop = [[FPPopoverController alloc]initWithViewController:demoVC];
    pop.tint = FPPopoverRedTint;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        pop.contentSize = CGSizeMake(360, 280);
    }else{
        pop.contentSize = CGSizeMake(200, 300);
        
    }
    pop.arrowDirection = FPPopoverArrowDirectionAny;
    // NSLog(@"sender.tag = %d",sender.tag);
    
    [pop presentPopoverFromView:sender];
    
}


- (void)SelectisKonow:(UIGestureRecognizer*)sender {
    
}
@end
