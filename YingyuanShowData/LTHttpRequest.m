//
//  LTHTTPNetwork.m
//  swcshd
//
//  Created by PsylifeMacBook on 14-5-4.
//  Copyright (c) 2014年 Psylife. All rights reserved.
//

#import "LTHttpRequest.h"

@implementation LTHttpRequest



-(void)askHttpGet:(NSString *)strURL
{
    NSURL *url = [NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"url:>%@", url);
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];

    if(conn)
    {
        _datas = [[NSMutableData alloc]init];
    }
}

-(void)askHttpPost:(NSString *)strURL prams:(NSDictionary *)postPrames
{
    NSLog(@" askHttpPost strURL \n %@",strURL);
    NSURL *url = [NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSMutableString *post =[[NSMutableString alloc]init];
    for (NSString *dicKey  in postPrames) {
        NSString *valitem = [postPrames objectForKey:dicKey];
        [post appendFormat:@"%@=%@&",dicKey,valitem];
    }
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    if(conn)
        _datas = [[NSMutableData alloc]init];

}


#pragma mark - NSURLConn huidiao
-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_datas appendData:data];
}
-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERR LOG:>%@",[error localizedDescription]);
    NSString *reString = [error localizedDescription];
    if ([reString isEqualToString:@"Could not connect to the server."]) {
        reString = @"无法连接服务器";
    }
    if (self.delegate) {
        [self.delegate requestFinishedWithErr:reString];
    }


}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
   NSString * dic = [[NSString alloc]initWithData:_datas encoding: NSUTF8StringEncoding];
    
    NSLog(@"dic =================================== %@",dic);
    NSMutableDictionary *dic1 = [NSJSONSerialization JSONObjectWithData:_datas options:0 error:nil];
//    NSLog(@"ddddddd%@",dic1);

//    NSLog(@"connectionDidFinishLoading %@",dic);
    if (self.delegate) {
        [self.delegate requestFinished:dic1 andData:dic];
    }
}

#pragma mark 静态初始化
+(LTHttpRequest *)requestWithDelegate:(id<LTHttpRequestDelegate >)delegate
{
    LTHttpRequest *tempreq =  [[LTHttpRequest alloc]init];
    tempreq.delegate = delegate;
    return tempreq;
}
/*
#pragma mark 自定义指定功能函数
-(void)checkVersion
{
    NSLog(@"_webaddress %@",_webaddress);
    [self askHttpGet:[NSString stringWithFormat:@"%@%@",_webaddress,@"CheckVer"]];
}


-(void)loginWith:(NSString *)username banji:(NSString *)banji nianji:(NSString *)nianji xiaoqu:(NSString *)xiaoqu
{
    [self askHttpPost: [NSString stringWithFormat:@"%@login.fx",_webaddress] prams:@{@"user_name":username,@"user_banji":banji,@"user_nianji":nianji}];
}

-(void)updataKcbWeek:(int)iweekId  when:(NSString *)ivar content:(NSString *)icentent
{
   NSString *user_uuid = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_uuid"];
    [self askHttpPost:[NSString stringWithFormat:@"%@kcb.fx",_webaddress]
                    prams:@{@"action":@"updateKCB",@"user_uuid":user_uuid,@"iweekId":[NSString stringWithFormat:@"%d",iweekId],@"ivar":ivar,@"icentent":icentent}];
}
-(void)checkWeekKCB:(int ) iweekId
{
    NSString *user_uuid = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_uuid"];
    [self askHttpPost:[NSString stringWithFormat:@"%@kcb.fx",_webaddress]
                prams:@{@"action":@"getMyKCB",@"user_uuid":user_uuid,@"iweekId":[NSString stringWithFormat:@"%d",iweekId]}];
}
-(void)upOneWeekKcb:(NSMutableDictionary *)oneWeekcb when:(int)iweekId
{
    NSString *user_uuid = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_uuid"];
    
    [oneWeekcb setObject:@"getMyKCB"  forKey:@"action"];
    [oneWeekcb addEntriesFromDictionary:@{@"action":@"upOneMyKCB",@"user_uuid":user_uuid,@"iweekId":[NSString stringWithFormat:@"%d",iweekId]}];
    
    NSLog(@"``````%@",oneWeekcb);
    
    [self askHttpPost:[NSString stringWithFormat:@"%@kcb.fx",_webaddress]
                prams:oneWeekcb];
}
*/
@end
