//
//  LTHTTPNetwork.h
//  swcshd
//
//  Created by PsylifeMacBook on 14-5-4.
//  Copyright (c) 2014年 Psylife. All rights reserved.
//

#import <Foundation/Foundation.h>



@protocol LTHttpRequestDelegate <NSObject>

@required
-(void)requestFinished:(NSDictionary*)response andData:(NSString*)data;
-(void)requestFinishedWithErr:(NSString *)errCode;

@end

@interface LTHttpRequest : NSObject<NSURLConnectionDataDelegate>
{
    NSMutableData * _datas;
}

@property (nonatomic,strong) id<LTHttpRequestDelegate> delegate;


+(LTHttpRequest *)requestWithDelegate:(id<LTHttpRequestDelegate >)delegate;


-(void)askHttpGet:(NSString *)strURL;
-(void)askHttpPost:(NSString *)strURL prams:(NSDictionary *)postPrames;

-(void)checkVersion;
-(void)loginWith:(NSString *)username banji:(NSString *)banji nianji:(NSString *)nianji xiaoqu:(NSString *)xiaoqu;
-(void)updataKcbWeek:(int)iweekId  when:(NSString *)ivar content:(NSString *)icentent;
-(void)checkWeekKCB:(int)iweekId;

-(void)upOneWeekKcb:(NSDictionary *)oneWeekcb when:(int)iweekId;


@end
