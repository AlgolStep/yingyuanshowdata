//
//  YYKnowledgeViewController.m
//  YingYuanMath
//
//  Created by 赵 峰 on 14-9-17.
//  Copyright (c) 2014年 psylife. All rights reserved.
//

#import "YYKnowledgeViewController.h"
//#import "YYMovieViewController.h"
#import "SubDemo2TableViewController.h"
@interface YYKnowledgeViewController ()
@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIButton *button3;
@property (strong, nonatomic) IBOutlet UIButton *button4;
@property (strong, nonatomic) IBOutlet UIButton *button5;
@property (strong, nonatomic) IBOutlet UIButton *button6;
@property (strong, nonatomic) IBOutlet UIButton *button7;
@property (strong, nonatomic) IBOutlet UIButton *button8;
@property (strong, nonatomic) IBOutlet UIButton *button9;
@property (strong, nonatomic) IBOutlet UIButton *button10;
@property (strong, nonatomic) IBOutlet UIButton *button11;
@property (strong, nonatomic) IBOutlet UIButton *button12;
@property (strong, nonatomic) IBOutlet UIButton *button13;


@end
@implementation YYKnowledgeViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    
}
-(void)saveLocalData:(NSString*)string andObject:(NSString*)obj{
    [[NSUserDefaults standardUserDefaults]setObject:obj forKey:string];
    [[NSUserDefaults standardUserDefaults]synchronize];
  
}

//
- (IBAction)clearLocalData:(UIButton *)sender {
    for (int i = 0; i < 13; i ++) {
        [self saveLocalData:[NSString stringWithFormat:@"button%d",i] andObject:@"0"];
        [self saveLocalData:[NSString stringWithFormat:@"unfinished%d",i] andObject:@"0"];
    }
    NSLog(@"清空按钮被点击！");
    
    [self unlockButton];
}

-(void)unlockButton{
    
   
    
    for (int i = 0; i < 13; i ++) {
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"button%d",i]]isEqualToString:@"1"]) {
            /**
             *******************************************************
             */
            if (i ==0) {
                
                self.button1.enabled = YES;
                [self.button1 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
                
            }else if (i ==1){
                
                self.button2.enabled = YES;
                [self.button2 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
                
            }else if (i == 2){
                self.button3.enabled = YES;
                [self.button3 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            
            }else if (i == 3){
            
                self.button4.enabled = YES;
                [self.button4 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            }else if (i == 4){
                
                self.button5.enabled = YES;
                [self.button5 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
                
            }else if(i == 5){
                self.button6.enabled = YES;
                [self.button6 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            
            }else if (i == 6){
                self.button7.enabled = YES;
                [self.button7 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            }else if (i == 7){
                
                self.button8.enabled = YES;
                [self.button8 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
                
            }else if(i == 8){
            
                self.button9.enabled = YES;
                [self.button9 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            }else if (i == 9){
            
                self.button10.enabled = YES;
                [self.button10 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            }else if (i == 10){
                
                self.button11.enabled = YES;
                [self.button11 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
                
            }else if (i == 11){
                
                self.button12.enabled = YES;
                [self.button12 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
                
            }else if (i == 12){
                self.button13.enabled = YES;
                [self.button13 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            
            }
            /**
            ***********************************************************
             */
        }else{
            /**
             *******************************************************
             */
            if (i ==0) {
                
                self.button1.enabled = YES;
                [self.button1 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                
            }else if (i ==1){
                
                self.button2.enabled = NO;
                [self.button2 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
                
            }else if (i == 2){
                self.button3.enabled = NO;
                [self.button3 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
                
            }else if (i == 3){
                
                self.button4.enabled = NO;
                [self.button4 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
            }else if (i == 4){
                
                self.button5.enabled = NO;
                [self.button5 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
                
            }else if(i == 5){
                self.button6.enabled = NO;
                [self.button6 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
                
            }else if (i == 6){
                self.button7.enabled = NO;
                [self.button7 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
            }else if (i == 7){
                
                self.button8.enabled = NO;
                [self.button8 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
                
            }else if(i == 8){
                
                self.button9.enabled = NO;
                [self.button9 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
            }else if (i == 9){
                
                self.button10.enabled = NO;
                [self.button10 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
            }else if (i == 10){
                
                self.button11.enabled = NO;
                [self.button11 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
                
            }else if (i == 11){
                self.button12.enabled = NO;
               [self.button12 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
                
            }else if (i == 12){
                
                self.button13.enabled = NO;
                [self.button13 setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];
            }

        
        
        }
    }
   
    for (int j = 0; j < 13; j ++) {
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"unfinished%d",j]]isEqualToString:@"1"]) {
            switch (j) {
                case 0:
                    
                    break;
                case 1:
                    self.button2.enabled = YES;
                    [self.button2 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 2:
                    self.button3.enabled = YES;
                    [self.button3 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 3:
                    self.button4.enabled = YES;
                    [self.button4 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 4:
                    self.button5.enabled = YES;
                    [self.button5 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 5:
                    self.button6.enabled = YES;
                    [self.button6 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 6:
                    self.button7.enabled = YES;
                    [self.button7 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 7:
                    self.button8.enabled = YES;
                    [self.button8 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 8:
                    self.button9.enabled = YES;
                    [self.button9 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 9:
                    self.button10.enabled = YES;
                    [self.button10 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 10:
                    self.button11.enabled = YES;
                    [self.button11 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 11:
                    self.button12.enabled = YES;
                    [self.button12 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                case 12:
                    self.button13.enabled = YES;
                    [self.button13 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
                    break;
                default:
                    break;
            }
        }
    }
    
    

}
-(void)changeButtonBackground:(NSNotification*)not{
    NSDictionary *dic = [not userInfo];
    NSString *imageTag = [dic objectForKey:@"buttontag"];
    
    switch ([imageTag integerValue]) {
        case 2:
            //按钮1完成
            [self.button1 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            [self saveLocalData:@"button0" andObject:@"1"];
            
            //开启按钮2
            [self.button2 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button2.enabled = YES;
            [self saveLocalData:@"unfinished1" andObject:@"1"];
            break;
        case 3:
            //按钮2完成
            [self.button2 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
             [self saveLocalData:@"button1" andObject:@"1"];
             [self saveLocalData:@"unfinished1" andObject:@"0"];
            
            //开启按钮3
            [self.button3 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button3.enabled = YES;
            [self saveLocalData:@"unfinished2" andObject:@"1"];
            
            break;
        case 4:
            //按钮3完成
            [self.button3 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
             [self saveLocalData:@"button2" andObject:@"1"];
            [self saveLocalData:@"unfinished2" andObject:@"0"];
            
            //开启按钮4,10
            [self.button4 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button4.enabled = YES;
            [self saveLocalData:@"unfinished3" andObject:@"1"];
            
            [self.button10 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button10.enabled = YES;
            [self saveLocalData:@"unfinished9" andObject:@"1"];
            break;
        case 5:
            //按钮4完成
            [self.button4 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            [self saveLocalData:@"button3" andObject:@"1"];
            [self saveLocalData:@"unfinished3" andObject:@"0"];
            //开启按钮5,6
            [self.button5 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button5.enabled = YES;
            [self saveLocalData:@"unfinished4" andObject:@"1"];
            
            [self.button6 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button6.enabled = YES;
           [self saveLocalData:@"unfinished5" andObject:@"1"];

            break;
        case 6:
            //按钮5完成
            [self.button5 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
             [self saveLocalData:@"button4" andObject:@"1"];
             [self saveLocalData:@"unfinished4" andObject:@"0"];
            break;
        case 7://开启按钮7,8,9.
            
            //按钮6完成
            [self.button6 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
             [self saveLocalData:@"button5" andObject:@"1"];
             [self saveLocalData:@"unfinished5" andObject:@"0"];
            //开启按钮7,8,9.
            [self.button7 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button7.enabled = YES;
            [self saveLocalData:@"unfinished6" andObject:@"1"];
            
            [self.button8 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button8.enabled = YES;
            [self saveLocalData:@"unfinished7" andObject:@"1"];
            
            [self.button9 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button9.enabled = YES;
           [self saveLocalData:@"unfinished8" andObject:@"1"];
            break;
            
        case 8:
            //按钮7完成
            [self.button7 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            [self saveLocalData:@"button6" andObject:@"1"];
            [self saveLocalData:@"unfinished6" andObject:@"0"];
            break;
        case 9:
            //按钮8完成
           [self.button8 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
             [self saveLocalData:@"button7" andObject:@"1"];
            [self saveLocalData:@"unfinished7" andObject:@"0"];
            break;
        case 10:
            //按钮9完成
            [self.button9 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            [self saveLocalData:@"button8" andObject:@"1"];
            [self saveLocalData:@"unfinished8" andObject:@"0"];
            break;
        case 11:
            //按钮10完成
            [self.button10 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            [self saveLocalData:@"button9" andObject:@"1"];
            [self saveLocalData:@"unfinished9" andObject:@"0"];
            //开启按钮11
            [self.button11 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button11.enabled = YES;
            [self saveLocalData:@"unfinished10" andObject:@"1"];
            break;
        case 12:
            //按钮11完成
            [self.button11 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
            [self saveLocalData:@"button10" andObject:@"1"];
            [self saveLocalData:@"unfinished10" andObject:@"0"];
            //开启按钮12,13
            [self.button12 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button12.enabled = YES;
            [self saveLocalData:@"unfinished11" andObject:@"1"];
            
            [self.button13 setImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
            self.button13.enabled = YES;
           [self saveLocalData:@"unfinished12" andObject:@"1"];
            
            break;
        case 13:
            //按钮12完成
             [self.button12 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
             [self saveLocalData:@"button11" andObject:@"1"];
             [self saveLocalData:@"unfinished11" andObject:@"0"];
            break;
        case 14:
            //按钮13完成
             [self.button13 setImage:[UIImage imageNamed:@"finish.png"] forState:UIControlStateNormal];
             [self saveLocalData:@"button12" andObject:@"1"];
            [self saveLocalData:@"unfinished12" andObject:@"0"];
            break;
        default:
            break;
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -- Event Method
- (IBAction)btnClick:(UIButton *)sender {
    NSLog(@"sender.tag =%d",sender.tag);
    
    
    if (pop != nil) {
        [pop dismissPopoverAnimated:NO];
    }
    SubDemo2TableViewController *demoVC = [[SubDemo2TableViewController alloc]initWithStyle:UITableViewStylePlain];

    pop = [[FPPopoverController alloc]initWithViewController:demoVC];
    pop.tint = FPPopoverWhiteTint;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        pop.contentSize = CGSizeMake(460, 280);
    }else{
        pop.contentSize = CGSizeMake(200, 300);
        
    }
    pop.arrowDirection = FPPopoverArrowDirectionAny;
    NSLog(@"sender.tag = %d",sender.tag);
    
    [pop presentPopoverFromView:sender];
    
}






- (IBAction)BackSuperView:(id)sender {
    
        [self dismissViewControllerAnimated:YES completion:nil];
}
@end
