//
//  SubDemoTableViewController.m
//  YingYuanMath
//
//  Created by Psylife_iPro02  on 14-11-25.
//  Copyright (c) 2014年 psylife. All rights reserved.
//

#import "SubDemoTableViewController.h"

@interface SubDemoTableViewController ()

@end

@implementation SubDemoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:224.0/255.0 blue:113.0/255.0 alpha:1.0f];
    if (indexPath.row == 0) {
         cell.textLabel.text = [NSString stringWithFormat:@"总访问量:556次 总喜欢程度:90％"];
        cell.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0];
    }else if(indexPath.row ==1){
        cell.textLabel.text =  [NSString stringWithFormat:@"6(2)班访问量:100次 喜欢程度:86％"];
    }else if (indexPath.row ==2){
       cell.textLabel.text =  [NSString stringWithFormat:@"6(3)班访问量:100次 喜欢程度:92％"];
    }else if (indexPath.row ==3){
        cell.textLabel.text = [NSString stringWithFormat:@"6(4)班访问量:100次 喜欢程度:90％"];
    }else if (indexPath.row ==4){
       cell.textLabel.text = [NSString stringWithFormat:@"6(8)班访问量:100次 喜欢程度:95％"];
    }else if (indexPath.row ==5){
        cell.textLabel.text = [NSString stringWithFormat:@"6(10)班访问量:156次 喜欢程度:85％"];
    }
    
    return cell;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
