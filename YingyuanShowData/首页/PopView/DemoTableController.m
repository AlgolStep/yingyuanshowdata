//
//  DemoTableControllerViewController.m
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import "DemoTableController.h"
#import "MainViewController.h"
@interface DemoTableController ()

@end

@implementation DemoTableController
@synthesize delegate=_delegate;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //self.title = @"Popover Title";
//    switch (self.btnTag) {
//        case 0:
//            self.title = @"检测提高";
//            break;
//        case 1:
//            self.title = @"你知道吗？";
//            break;
//        case 2:
//            self.title = @"个人数据";
//            break;
//        case 3:
//            self.title = @"错题本";
//            break;
//        case 4:
//            self.title = @"知识框架";
//            break;
//        case 5:
//            self.title = @"视频";
//            break;
//        default:
//            break;
//    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    if (indexPath.row == 0) {
        cell.textLabel.text = @"总访问量:500";
        cell.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0];
    }else if (indexPath.row == 1){
        
      cell.textLabel.text =@"近一周访问量";
      cell.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:224.0/255.0 blue:113.0/255.0 alpha:1.0f];
        
    }else{
        if (self.btnTag !=2&&self.btnTag!=3) {
            cell.backgroundColor = [UIColor colorWithRed:133.0/255.0 green:218.0/255.0 blue:233.0/255.0 alpha:1.0];
            cell.textLabel.text = @"进入二级页面";
        }
        
    }
    
    
    return cell;
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([self.delegate respondsToSelector:@selector(selectedTableRow:)])
    {
        [self.delegate selectedTableRow:indexPath.row];
    }
}



@end
