//
//  XYZChartViewController.m
//  YingYuanMath
//
//  Created by Psylife_iPro02  on 14-12-1.
//  Copyright (c) 2014年 psylife. All rights reserved.
//

#import "XYZChartViewController.h"
#import "EColumView.h"
//#import "Config.h"
//#import "DateByItemView.h"
//#import "PNChart.h"

#import "PNLineChartView.h"
#import "PNPlot.h"
@interface XYZChartViewController ()
@property (strong, nonatomic) IBOutlet UIScrollView *chartScrollView;

@property (strong, nonatomic) IBOutlet PNLineChartView *lineChartView;

@end

@implementation XYZChartViewController
-(BOOL)prefersStatusBarHidden{
    
    return YES;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //hide status bar
    [[UIApplication sharedApplication]setStatusBarHidden:YES];
    
    //1.方法2
    
    [self addChartView1];

    
    //2.方法2
    //[self  addChartView2];
    
    //3.方法3
    //[self addChartView3];
    
    //4.方法4
    [self addChartView4];
    
}
#pragma mark -- Custom Method
-(void)addChartView1{
    //    //日访问表
        EColumView *ecView1 = [[EColumView alloc]initWithFrame:CGRectMake(100, 80, 800, 280)];
        ecView1.backgroundColor = [UIColor clearColor];
        [self.view addSubview:ecView1];
    //
    //    //月访问表
    //    EColumView *ecView2 = [[EColumView alloc]initWithFrame:CGRectMake(100, 435, 800, 280)];
    //    ecView2.backgroundColor = [UIColor clearColor];
    //    [self.view addSubview:ecView2];
    
    
    
}


-(void)addChartView4{
 
    // test line chart
    NSArray* plottingDataValues1 =@[@27, @33, @20, @168,@43, @32,@53];
    
    
    self.lineChartView.max = 180;
    self.lineChartView.min = 0;
    
    
    self.lineChartView.interval = (self.lineChartView.max-self.lineChartView.min)/5;
    
    NSMutableArray* yAxisValues = [@[] mutableCopy];
    for (int i=0; i<6; i++) {
        NSString* str = [NSString stringWithFormat:@"%d次",(int)(self.lineChartView.min+self.lineChartView.interval*i)];
        [yAxisValues addObject:str];
    }
    
    self.lineChartView.xAxisValues = @[@"12-01", @"12-02", @"12-03",@"12-04", @"12-05", @"12-06",@"12-07"];
    self.lineChartView.yAxisValues = yAxisValues;
    self.lineChartView.axisLeftLineWidth = 39;
    
    
    PNPlot *plot1 = [[PNPlot alloc] init];
    plot1.plottingValues = plottingDataValues1;
    
    plot1.lineColor = [UIColor orangeColor];
    plot1.lineWidth = 2;
    
    [self.lineChartView addPlot:plot1];

    



}
- (IBAction)dismissAction:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
