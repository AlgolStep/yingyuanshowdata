//
//  SubDemo1TableViewController.m
//  YingYuanMath
//
//  Created by Psylife_iPro02  on 14-12-4.
//  Copyright (c) 2014年 psylife. All rights reserved.
//

#import "SubDemo1TableViewController.h"

@interface SubDemo1TableViewController ()
{
    NSString *title;
}
@end

@implementation SubDemo1TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.t == 1) {
        title = @"简单";
    }else if(self.t == 2){
        title = @"困难";
    }else if (self.t == 3){
       title = @"中等";
    
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return 21;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:224.0/255.0 blue:113.0/255.0 alpha:1.0f];
    if (indexPath.row == 0) {
        cell.textLabel.text = [NSString stringWithFormat:@"总访问量:556次 总正确率:90％"];
        cell.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0];
    }else{
        cell.textLabel.text = [NSString stringWithFormat:@"%@%d访问量:556次 正确率:90％",title,indexPath.row];
    }
    return cell;

    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
