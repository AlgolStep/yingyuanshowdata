//
//  SubDemo2TableViewController.m
//  YingYuanMath
//
//  Created by Psylife_iPro02  on 14-12-4.
//  Copyright (c) 2014年 psylife. All rights reserved.
//

#import "SubDemo2TableViewController.h"

@interface SubDemo2TableViewController ()

@end

@implementation SubDemo2TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return 6;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor = [UIColor colorWithRed:207.0/255.0 green:224.0/255.0 blue:113.0/255.0 alpha:1.0f];
    if (indexPath.row == 0) {
        cell.textLabel.text = [NSString stringWithFormat:@"总访问量:556次"];
        cell.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0];
    }else if(indexPath.row ==1){
        cell.textLabel.text =  [NSString stringWithFormat:@"6(2)班访问量:100次 视频播放次数:109 正确率:90％"];
    }else if (indexPath.row ==2){
         cell.textLabel.text =  [NSString stringWithFormat:@"6(3)班访问量:100次 视频播放次数:100 正确率:90％"];
    }else if (indexPath.row ==3){
         cell.textLabel.text =  [NSString stringWithFormat:@"6(4)班访问量:100次 视频播放次数:86 正确率:90％"];
    }else if (indexPath.row ==4){
         cell.textLabel.text =  [NSString stringWithFormat:@"6(8)班访问量:100次 视频播放次数:78 正确率:90％"];
    }else if (indexPath.row ==5){
        cell.textLabel.text =  [NSString stringWithFormat:@"6(10)班访问量:100次 视频播放次数:86  正确率:90％"];
    }
    
    return cell;

}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
