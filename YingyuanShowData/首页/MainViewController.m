//
//  MainViewController.m
//  YingYuanMath
//
//  Created by 王正魁 on 14-9-17.
//  Copyright (c) 2014年 psylife. All rights reserved.
//

#import "MainViewController.h"
#import "DemoTableController.h"
#import "XYZChartViewController.h"
#import "TitleViewController.h"
#import "StudentViewController.h"
#import "DataViewController.h"


@interface MainViewController (){
    NSInteger btnTag;
    CGRect originViewFrame;
    costumView *ccView;
}
@property (strong, nonatomic) IBOutlet UIImageView *imageForBIG;
@property (strong, nonatomic) IBOutlet UIButton *showViewButton;

@end

@implementation MainViewController
- (IBAction)buttonForLook:(id)sender {
    self.imageForBIG.hidden = NO;
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear");
}
-(void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"viewWillDisappear");
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


- (IBAction)buttonSelect:(UIButton *)sender {
    //方法二
    btnTag = sender.tag;
    //
    if (pop != nil) {
        [pop dismissPopoverAnimated:NO];
    }
    DemoTableController *demoVC = [[DemoTableController alloc]initWithStyle:UITableViewStylePlain];
    demoVC.btnTag = sender.tag;//标题
    demoVC.delegate = self;
    pop = [[FPPopoverController alloc]initWithViewController:demoVC];
    pop.tint = FPPopoverWhiteTint;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        pop.contentSize = CGSizeMake(300, 200);
    }else{
        pop.contentSize = CGSizeMake(200, 300);
    
    }
    pop.arrowDirection = FPPopoverArrowDirectionAny;
    NSLog(@"sender.tag = %d",sender.tag);
    
    [pop presentPopoverFromView:sender];
    
    
}
- (IBAction)selectedBtnClick:(UIButton *)sender {
    
}


#pragma mark -- Custom Method
-(void)selectedTableRow:(NSUInteger)rowNum
{
    NSLog(@"SELECTED ROW %d",rowNum);
    [pop dismissPopoverAnimated:NO];
    if (rowNum == 1) {//进入访问量页面
        XYZChartViewController *chartVC = [[XYZChartViewController alloc]init];
        chartVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:chartVC animated:YES completion:nil];
        
        
    }else if (rowNum == 2){
        
    [self gotoSubModel:btnTag];//进入二级页面
        
    }
    }
-(void)gotoSubModel:(NSInteger)i{
    switch (i) {/**** 中间卡顿时间是让动画执行，动画结束后在执行页面跳转 ****/
        case 0://检测提高
        {
            
            EnhanceViewController* enhance = [[EnhanceViewController alloc] initWithNibName:@"EnhanceViewController" bundle:[NSBundle mainBundle]];
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:enhance];
            [nav setNavigationBarHidden:YES];
            nav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:nav animated:YES completion:^{
            }];
        }
            break;
        case 1://你知道吗？
        {
            
            //[NSThread sleepForTimeInterval:0.35];
            YYIsKonwViewController* enhance = [[YYIsKonwViewController alloc] init];
            
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:enhance];
            [nav setNavigationBarHidden:YES];
            nav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:nav animated:YES completion:^{
            }];
            
            
        }
            break;
        case 2://个人数据
        {
            
//            //[NSThread sleepForTimeInterval:1];
//            YYPerDataViewController* enhance = [[YYPerDataViewController alloc] init];
//            
//            //保存时间
//            enhance.time = [LTModelTxtPen stringFromDate];
//            
//            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:enhance];
//            [nav setNavigationBarHidden:YES];
//            nav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//            [self presentViewController:nav animated:YES completion:^{
//            }];

            
        }
            break;
        case 3://错题本
        {
            
//            //[NSThread sleepForTimeInterval:0.8];
//            CuoTiViewController* cuo = [[CuoTiViewController alloc] init];
//            //保存时间
//            cuo.time = [LTModelTxtPen stringFromDate];
//            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:cuo];
//            [nav setNavigationBarHidden:YES];
//            nav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//            [self presentViewController:nav animated:YES completion:^{
//            }];

        }
            break;
        case 4://知识框架
        {
            
            //[NSThread sleepForTimeInterval:0.7];
            YYKnowledgeViewController* enhance = [[YYKnowledgeViewController alloc] init];
            
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:enhance];
            [nav setNavigationBarHidden:YES];
            nav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:nav animated:YES completion:^{
            }];
        }
            break;
        case 5://视频
        {
            
            //[NSThread sleepForTimeInterval:0.7];
            YYVideoPlayViewController* video = [[YYVideoPlayViewController alloc] init];
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:video];
            [nav setNavigationBarHidden:YES];
            nav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:nav animated:YES completion:^{
            }];
           
        }
            break;
        default:
            break;
    }


}
-(void)hiddenForImage
{
    self.imageForBIG.hidden = YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    ccView = [[costumView alloc]initWithFrame:CGRectMake(0, -48, 1024, 48)];
    ccView.delegate = self;
    originViewFrame = ccView.frame;
    [ccView.buttonA setImage:[UIImage imageNamed:@"触发按环节"] forState:UIControlStateNormal];
    [self.view addSubview:ccView];
    index = 0;
    
    //引导图只执行一次
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"YingYuan"]) {
         self.imageForBIG.hidden = NO;
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"YingYuan"];
    }
   
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenForImage)];
    [self.imageForBIG addGestureRecognizer:tap];
}

#pragma mark -- Method Event
- (IBAction)dismissButtonClicl:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Action

- (IBAction)onShowViewClick:(id)sender {
    [self showDataView];
    self.showViewButton.hidden = YES;
}

#pragma mark - ButtonChangeViewController Delegate

- (void)changeToCircleViewController
{
//    MainViewController *mainViewController = [[MainViewController alloc]initWithNibName:@"MainViewController" bundle:nil];
//    [self presentViewController:mainViewController animated:YES completion:nil];
}
- (void)changeToStudentViewController
{
    StudentViewController *studentViewController = [[StudentViewController alloc]initWithNibName:@"StudentViewController" bundle:nil];
    [self presentViewController:studentViewController animated:YES completion:nil];
}
- (void)changeToTitleViewController
{
    TitleViewController *titleViewController = [[TitleViewController alloc]initWithNibName:@"TitleViewController" bundle:nil];
    [self presentViewController:titleViewController animated:YES completion:nil];
}
- (void)changeToDataViewController
{
    DataViewController *dataViewController = [[DataViewController alloc]initWithNibName:@"DataViewController" bundle:nil];
    [self presentViewController:dataViewController animated:YES completion:nil];
}

-(void)hideView
{
    [self hideDataView];
    self.showViewButton.hidden = NO;
}

- (void)showDataView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    CGRect newFrame = CGRectMake(ccView.frame.origin.x, 0, ccView.frame.size.width, ccView.frame.size.height);
    ccView.frame = newFrame;
    ccView.hidden = NO;
    [UIView commitAnimations];
}

- (void)hideDataView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    ccView.frame = originViewFrame;
    //    self.showView.hidden = YES;
    [UIView commitAnimations];
}



@end
