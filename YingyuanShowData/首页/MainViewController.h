//
//  MainViewController.h
//  YingYuanMath
//
//  Created by 王正魁 on 14-9-17.
//  Copyright (c) 2014年 psylife. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EnhanceViewController.h"
#import "YYKnowledgeViewController.h"
#import "YYIsKonwViewController.h"
#import "YYVideoPlayViewController.h"
#import "costumView.h"

@interface MainViewController : UIViewController<ButtonChangeViewControllerDelegate>
{
     UIViewController* animetionViewController;
    FPPopoverController *pop;
    int index;
    UIButton* buttons;
}
@property(nonatomic,strong)IBOutlet UIButton* buttonForYouKnow;
@property (strong, nonatomic) IBOutlet UIButton *buttonStudentData;
@property (strong, nonatomic) IBOutlet UIButton *buttonErrorNotebook;
@property (strong, nonatomic) IBOutlet UIButton *buttonKnowledgeFramework;
@property (strong, nonatomic) IBOutlet UIButton *buttonVideo;
@property (strong, nonatomic) IBOutlet UIButton *buttonTestImproved;





//选中某行
-(void)selectedTableRow:(NSUInteger)rowNum;

@end
