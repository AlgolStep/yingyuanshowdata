//
//  LoginViewController.m
//  YingyuanShowData
//
//  Created by 思来氏 on 14-11-24.
//  Copyright (c) 2014年 PSYDemo. All rights reserved.
//

#import "LoginViewController.h"
#import "LTHttpRequest.h"
#import "StudentViewController.h"

#define DATAURL @"http://192.168.1.134:5080/CommonBack/test"

@interface LoginViewController ()
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;

@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIView *loginView;


@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@end

@implementation LoginViewController
{
    CGRect originFrame;
    NSTimer *timer;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    originFrame = self.loginView.frame;
    self.passwordTextField.delegate = self;
    self.nameTextField.delegate = self;
    self.progressView.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (IBAction)onLoginBtnClick:(id)sender {

    self.view.userInteractionEnabled = NO;
     LTHttpRequest *http = [[LTHttpRequest alloc]init];
    [http askHttpGet:DATAURL];
    http.delegate = self;
    self.nameTextField.text = @"妹子";
    [[NSUserDefaults standardUserDefaults] setObject:self.nameTextField.text forKey:@"userName"];
    
    self.progressView.hidden = NO;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(onProgressShow) userInfo:nil repeats:YES];
}

- (void)onProgressShow
{
    
    NSLog(@"the number%f",self.progressView.progress);
    if (self.progressView.progress ==1.0) {
        self.progressView.progress = 0.0;
    }else{
        self.progressView.progress = self.progressView.progress + 0.01;
    }
}

-(void)requestFinished:(NSDictionary*)response andData:(NSString*)data{
    
    [[NSUserDefaults standardUserDefaults]setObject:response forKey:@"LocalDic"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    self.view.userInteractionEnabled = YES;
    [timer invalidate];
    StudentViewController *studentVCtrl = [[StudentViewController alloc]initWithNibName:@"StudentViewController" bundle:nil];
    
    [self presentViewController:studentVCtrl animated:YES completion:nil];
}

-(void)requestFinishedWithErr:(NSString *)errCode{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:errCode delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertView show];
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self upView];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    return YES;
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    [self downView];
}


- (void)upView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    CGRect newFrame = CGRectMake(0, -100, self.loginView.frame.size.width, self.loginView.frame.size.height) ;
    self.loginView.frame = newFrame;
    [UIView commitAnimations];
}

- (void)downView
{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    self.loginView.frame = originFrame;
    [UIView commitAnimations];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
