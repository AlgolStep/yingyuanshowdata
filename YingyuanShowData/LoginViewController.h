//
//  LoginViewController.h
//  YingyuanShowData
//
//  Created by 思来氏 on 14-11-24.
//  Copyright (c) 2014年 PSYDemo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTHttpRequest.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate,LTHttpRequestDelegate>

@end
