//
//  TitleViewController.h
//  YingyuanShowData
//
//  Created by 思来氏 on 14-11-25.
//  Copyright (c) 2014年 PSYDemo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSYPieChart.h"
#import "costumView.h"

@interface TitleViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,XYPieChartDataSource,XYPieChartDelegate,ButtonChangeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet PSYPieChart *pieChartView;
@property (strong, nonatomic) IBOutlet PSYPieChart *pieChartRightView;
@property (strong, nonatomic) IBOutlet UILabel *percentageRightLable;
@property (strong, nonatomic) IBOutlet UILabel *percentageLabel;

@property (nonatomic, strong) NSArray        *sliceColors;
@property (nonatomic, strong) NSArray        *sliceRightColors;
@property (nonatomic, strong) NSDictionary *dataDictionary;
@property (nonatomic, strong) NSArray *pdfNameArray;
@property (nonatomic, strong) NSArray *questionsNameArray;
@property (nonatomic, strong) NSArray *slicesArray;
@property (nonatomic, strong) NSArray *slicesNameArray;
@end
