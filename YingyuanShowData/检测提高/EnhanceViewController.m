//
//  EnhanceViewController.m
//  YingYuanMath
//
//  Created by 王正魁 on 14-9-18.
//  Copyright (c) 2014年 psylife. All rights reserved.
//

#import "EnhanceViewController.h"
#import "SubDemo1TableViewController.h"
@interface EnhanceViewController ()
@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UILabel *label3;
@property (strong, nonatomic) IBOutlet UILabel *label4;

@property (strong, nonatomic) IBOutlet UIImageView *easyImage1;
@property (strong, nonatomic) IBOutlet UIImageView *easyImage2;
@property (strong, nonatomic) IBOutlet UIImageView *easyImage3;
@property (strong, nonatomic) IBOutlet UIImageView *mediumImage1;
@property (strong, nonatomic) IBOutlet UIImageView *mediumImage2;
@property (strong, nonatomic) IBOutlet UIImageView *mediumImage3;

@property (strong, nonatomic) IBOutlet UIImageView *hardImage1;
@property (strong, nonatomic) IBOutlet UIImageView *hardImage2;
@property (strong, nonatomic) IBOutlet UIImageView *hardImage3;

@property (strong, nonatomic) IBOutlet UIImageView *limitlessImage1;
@property (strong, nonatomic) IBOutlet UIImageView *limitlessImage2;
@property (strong, nonatomic) IBOutlet UIImageView *limitlessImage3;

@property (strong, nonatomic) IBOutlet UILabel *theThisOneLabel;
@property (strong, nonatomic) IBOutlet UILabel *theBestOneLabel;


@end

@implementation EnhanceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)buttonSelect:(UIButton *)sender {

    NSLog(@"sender.tag =%d",sender.tag);
    if (sender.tag == 4) {
        return;
    }
    
    if (pop != nil) {
        [pop dismissPopoverAnimated:NO];
    }
    SubDemo1TableViewController *demoVC = [[SubDemo1TableViewController alloc]initWithStyle:UITableViewStylePlain];
    demoVC.t = sender.tag;
    pop = [[FPPopoverController alloc]initWithViewController:demoVC];
    pop.tint = FPPopoverRedTint;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        pop.contentSize = CGSizeMake(350, 250);
    }else{
        pop.contentSize = CGSizeMake(200, 300);
        
    }
    pop.arrowDirection = FPPopoverArrowDirectionAny;
    NSLog(@"sender.tag = %d",sender.tag);
    
    [pop presentPopoverFromView:sender];
    
    
    

}
- (IBAction)back:(id)sender {
    
    self.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //保存最高记录
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"BestOne"]) {
        self.theBestOneLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"BestOne"];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
