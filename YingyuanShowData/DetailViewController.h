//
//  DetailViewController.h
//  chart2221
//
//  Created by 思来氏 on 14-10-22.
//  Copyright (c) 2014年 com.ELS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASFTableView.h"
@interface DetailViewController : UIViewController
@property (strong, nonatomic) ASFTableView *mASFTableView;
@property (strong, nonatomic) NSArray *titleArray;
@property (strong, nonatomic) NSMutableArray *contentArray;
@end
