//
//  StudentViewController.m
//  YingyuanShowData
//
//  Created by 思来氏 on 14-11-25.
//  Copyright (c) 2014年 PSYDemo. All rights reserved.
//

#import "StudentViewController.h"
#import "TitleViewController.h"
#import "DataViewController.h"
#import "DetailViewController.h"
#import "MainViewController.h"

enum AboutTable{
    kClassTableView = 100001
};

@interface StudentViewController ()
@property (strong, nonatomic) IBOutlet UIButton *showViewButton;
@property (strong, nonatomic) IBOutlet UITableView *tableViewStudent;
@property (strong, nonatomic) IBOutlet UITableView *tableViewClass;
@property (strong, nonatomic) IBOutlet UILabel *lableShow;
@end

@implementation StudentViewController
{
    CGRect originViewFrame;
    costumView *ccView;
    NSString *contentString;
    DetailViewController *detailCtrl;
    BOOL firstShow;
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
        self.infoDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"LocalDic"];
        self.teacherDictionary = [self.infoDictionary objectForKey:userName];
        self.classArray = [self.teacherDictionary allKeys];
        if (self.classArray.count == 0) {
            
        }else{
            self.studentsDictionary = [self.teacherDictionary objectForKey:self.classArray[0]];
            self.studentsNameArray = [self.studentsDictionary allKeys];
        }
        
    }
    return self;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    firstShow = YES;
    
    ccView = [[costumView alloc]initWithFrame:CGRectMake(0, -48, 1024, 48)];
    ccView.delegate = self;
    originViewFrame = ccView.frame;
    [ccView.buttonB setImage:[UIImage imageNamed:@"触发按学生"] forState:UIControlStateNormal];
    [self.view addSubview:ccView];
    
    self.lableShow.text = @"迎园中学";
    self.lableShow.backgroundColor = [UIColor colorWithRed:0.71 green:0.78 blue:0.45 alpha:1.0];
    self.tableViewStudent.delegate = self;
    self.tableViewStudent.dataSource = self;
    [self.tableViewStudent setSeparatorColor:[UIColor whiteColor]];
    
    self.tableViewClass.delegate = self;
    self.tableViewClass.dataSource = self;
    self.tableViewClass.tag = kClassTableView;
    [self.tableViewClass setSeparatorColor:[UIColor whiteColor]];
    
    self.slicesArray = @[@"50",@"50"];
    self.slicesNameArray = @[@"掌握度",@"未掌握度"];
    self.slicesRightArray = @[@50,@50];
    self.slicesRightNameArray = @[@"探索度",@"未探索度"];
    
    [self.pieChartView setDelegate:self];
    [self.pieChartView setDataSource:self];
    [self.pieChartView setStartPieAngle:M_PI_2];
    [self.pieChartView setAnimationSpeed:1.0];
    [self.pieChartView setLabelFont:[UIFont fontWithName:@"DBLCDTempBlack" size:16]];
    [self.pieChartView setLabelRadius:110];
    [self.pieChartView setShowPercentage:YES];
    [self.pieChartView setPieBackgroundColor:[UIColor colorWithWhite:1.0 alpha:1]];
    [self.pieChartView setPieCenter:CGPointMake(190, 190)];
    [self.pieChartView setUserInteractionEnabled:YES];
    self.percentageLabel.layer.cornerRadius = 50.0f;
    self.percentageLabel.text = @"掌握度";
    self.percentageLabel.textColor = [UIColor colorWithRed:68/255.0 green:128/255.0 blue:150/255.0 alpha:1.0];
    
    [self.pieChartRightView setDataSource:self];
    [self.pieChartRightView setStartPieAngle:M_PI_2];
    [self.pieChartRightView setAnimationSpeed:1.0];
    [self.pieChartRightView setLabelFont:[UIFont fontWithName:@"DBLCDTempBlack" size:16]];
    [self.pieChartRightView setLabelRadius:110];
    [self.pieChartRightView setShowPercentage:YES];
    [self.pieChartRightView setPieBackgroundColor:[UIColor colorWithWhite:1.0 alpha:1]];
    [self.pieChartRightView setPieCenter:CGPointMake(190, 190)];
    [self.pieChartRightView setUserInteractionEnabled:YES];
    self.pieChartRightView.tag = 10001;
    self.percentageRightLable.layer.cornerRadius = 50.0f;
    self.percentageRightLable.text = @"探索度";
    self.percentageRightLable.textColor = [UIColor colorWithRed:68/255.0 green:128/255.0 blue:150/255.0 alpha:1.0];
    
    self.sliceColors =[NSArray arrayWithObjects:
                       [UIColor colorWithRed:253/255.0 green:110/255.0 blue:82/255.0 alpha:1],
                       [UIColor colorWithRed:253/255.0 green:174/255.0 blue:83/255.0 alpha:1],
                       [UIColor colorWithRed:62/255.0 green:173/255.0 blue:219/255.0 alpha:1],
                       [UIColor colorWithRed:229/255.0 green:66/255.0 blue:115/255.0 alpha:1],
                       [UIColor colorWithRed:148/255.0 green:141/255.0 blue:139/255.0 alpha:1],nil];
    
    self.sliceRightColors =[NSArray arrayWithObjects:
                            [UIColor colorWithRed:84/255.0 green:101/255.0 blue:126/255.0 alpha:1],
                            [UIColor colorWithRed:40/255.0 green:171/255.0 blue:187/255.0 alpha:1],
                            [UIColor colorWithRed:125/255.0 green:176/255.0 blue:50/255.0 alpha:1],
                            [UIColor colorWithRed:229/255.0 green:66/255.0 blue:105/255.0 alpha:1],
                            [UIColor colorWithRed:128/255.0 green:81/255.0 blue:39/255.0 alpha:1],nil];

    detailCtrl = [[DetailViewController alloc] init];
//    初始化显示第一个班级的 第一个人的数据
    self.studentsDictionary = [self.teacherDictionary objectForKey:self.classArray[0]];
    self.studentsNameArray = [self.studentsDictionary allKeys];
    self.studentDataArray = [self.studentsDictionary objectForKey:self.studentsNameArray[0]];
    
    detailCtrl.contentArray = [[NSMutableArray alloc]initWithCapacity:42];
    for (int i = 2; i < self.studentDataArray.count; i++) {
        [detailCtrl.contentArray addObject:self.studentDataArray[i]];
    }
    
    CGRect dataTableFrame = CGRectMake(180, 440, 800, 328);
    detailCtrl.view.frame = dataTableFrame;
    [self.view addSubview:detailCtrl.view];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self.pieChartView reloadData];
    [self.pieChartRightView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == kClassTableView) {
        return self.classArray.count;
    }else{
        return self.studentsNameArray.count;
    }
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if ( nil == cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    if (firstShow) {
        if (indexPath.row == 1) {
            [cell setSelected:YES animated:NO];
        }
    }
    
    if (tableView.tag == kClassTableView) {
        contentString = self.classArray[indexPath.row];
        cell.backgroundColor = [UIColor clearColor];
        UIColor *ccColor = [UIColor lightGrayColor];//通过RGB来定义自己的颜色
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = ccColor;
    }else{
        contentString = self.studentsNameArray[indexPath.row];
        cell.backgroundColor = [UIColor clearColor];
        UIColor *ccColor = [UIColor colorWithRed:0.85 green:0.92 blue:0.58 alpha:1.0];//通过RGB来定义自己的颜色
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = ccColor;
       
    }
    cell.textLabel.text = contentString;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont systemFontOfSize:20.0f];
    return cell;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    firstShow = NO;
    if (tableView.tag == kClassTableView) {
        self.studentsDictionary = [self.teacherDictionary objectForKey:self.classArray[indexPath.row]];
        self.studentsNameArray = [self.studentsDictionary allKeys];
        [self.tableViewStudent reloadData];
    }else{
        
        self.studentDataArray = [self.studentsDictionary objectForKey:self.studentsNameArray[indexPath.row]];
        if (self.studentDataArray.count <2) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"学生数据有误！" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            return;
        }
        self.masterDictionary = self.studentDataArray[0];
        self.exploreDictionary = self.studentDataArray[1];
        float masterPercent = [[self.masterDictionary objectForKey:@"neirong"] floatValue];
        NSString *masterOneData = [NSString stringWithFormat:@"%.2f",masterPercent];
        NSString *masterTwoData = [NSString stringWithFormat:@"%.2f",(100-masterPercent)];
        self.slicesArray = @[masterOneData,masterTwoData];
        
        
        float explorePercent = [[self.exploreDictionary objectForKey:@"neirong"] floatValue];
        NSString *exploreOneData = [NSString stringWithFormat:@"%.2f",explorePercent];
        NSString *exploreTwoData = [NSString stringWithFormat:@"%.2f",(100-explorePercent)];
        self.slicesRightArray = @[exploreOneData,exploreTwoData];
        
        [self.pieChartView reloadData];
        [self.pieChartRightView reloadData];
        
        detailCtrl.contentArray = [[NSMutableArray alloc]initWithCapacity:42];
        for (int i = 2; i < self.studentDataArray.count; i++) {
            [detailCtrl.contentArray addObject:self.studentDataArray[i]];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTableViewData" object:self userInfo:nil];
    }
    
    
}

#pragma mark - XYPieChart Data Source

- (NSUInteger)numberOfSlicesInPieChart:(PSYPieChart *)pieChart
{
    if (pieChart.tag == 10001) {
        return self.slicesRightArray.count;
    }else{
        return self.slicesArray.count;
    }
}

- (CGFloat)pieChart:(PSYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    if (pieChart.tag == 10001) {
        return [[self.slicesRightArray objectAtIndex:index] floatValue];
    }else{
        return [[self.slicesArray objectAtIndex:index] floatValue];
    }
    
    
}

- (UIColor *)pieChart:(PSYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    if (pieChart.tag == 10001) {
        return [self.sliceRightColors objectAtIndex:(index % self.sliceRightColors.count)];
    }
    return [self.sliceColors objectAtIndex:(index % self.sliceColors.count)];
}

- (NSArray*)pieChartNameArray:(PSYPieChart *)pieChart
{
    if (pieChart.tag == 10001) {
        return self.slicesRightNameArray;
    }else{
        return self.slicesNameArray;
    }
}


#pragma mark - XYPieChart Delegate
- (void)pieChart:(PSYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
{
    NSLog(@"did select slice at index %d",index);
//    if (pieChart.tag == 10001) {
//        NSString *tempStr =  @"%";
//        self.percentageLabel.text = [NSString stringWithFormat:@"%.2f%@",[[self.slicesArray objectAtIndex:index] floatValue],tempStr];
//    }
}

#pragma mark - Button Action

- (IBAction)onShowViewClick:(id)sender {
    [self showDataView];
    self.showViewButton.hidden = YES;
}



- (void)showDataView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    CGRect newFrame = CGRectMake(ccView.frame.origin.x, 0, ccView.frame.size.width, ccView.frame.size.height);
    ccView.frame = newFrame;
    ccView.hidden = NO;
    [UIView commitAnimations];
}

- (void)hideDataView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    ccView.frame = originViewFrame;
    //    self.showView.hidden = YES;
    [UIView commitAnimations];
}


#pragma mark - ButtonChangeViewController Delegate

- (void)changeToCircleViewController
{
    MainViewController *mainViewController = [[MainViewController alloc]initWithNibName:@"MainViewController" bundle:nil];
    [self presentViewController:mainViewController animated:YES completion:nil];
}
- (void)changeToStudentViewController
{
//    StudentViewController *studentViewController = [[StudentViewController alloc]initWithNibName:@"StudentViewController" bundle:nil];
//    [self presentViewController:studentViewController animated:YES completion:nil];
}
- (void)changeToTitleViewController
{
    TitleViewController *titleViewController = [[TitleViewController alloc]initWithNibName:@"TitleViewController" bundle:nil];
    [self presentViewController:titleViewController animated:YES completion:nil];
}
- (void)changeToDataViewController
{
    DataViewController *dataViewController = [[DataViewController alloc]initWithNibName:@"DataViewController" bundle:nil];
    [self presentViewController:dataViewController animated:YES completion:nil];
}

- (void)hideView
{
    [self hideDataView];
    self.showViewButton.hidden = NO;
}

@end
